# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect

from microblogging.models import Post
from microblogging.forms import SignUpForm, NewPost
from django.contrib.auth import login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.shortcuts import get_object_or_404


# Create your views here.

def index(request):
    post = Post.objects.all().order_by('-update_date')
    return render(request, 'index.html', {'latests_posts': post})

def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('microblogging:index')
    else:
        form = SignUpForm()
    return render(request, 'register.html', {'form': form})


def log_in(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('microblogging:index')
    else:
        form = AuthenticationForm()
    return render(request, 'log_in.html', {'form':form})

def logout_view(request):
    logout(request)
    return redirect('microblogging:index')
    
def post_detail(request, pk):
    post = get_object_or_404(Post,pk=pk) #recup donnée dans la bdd
    return render(request, 'post.html', {'post':post}) 

def new_post(request):
    form = NewPost(data=request.POST)
    if form.is_valid():
        result = form.save(commit=False)
        result.author = request.user
        result.save()
        return redirect('microblogging:index')
    else:
        form = NewPost()
    return render(request, 'newpost.html', {'form':form})


def post_delete(request, pk):
    post = get_object_or_404(Post,pk=pk)
    post.delete()
    return redirect('microblogging:index') 