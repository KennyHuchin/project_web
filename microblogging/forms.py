from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from microblogging.models import Post


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='')
    last_name = forms.CharField(max_length=30, required=True, help_text='')
    email = forms.EmailField(max_length=254, required=True, help_text='')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )


class NewPost(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'body')