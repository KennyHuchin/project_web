# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify

# Create your models here.


#class Users(models.Model):
#    username = models.CharField(max_length=20, null=False, blank=False)
#    password = models.CharField(max_length=100, null=False, blank=False)
#    first_name = models.CharField(max_length=20, null=True, blank=False)
#    last_name = models.CharField(max_length=20, null=True, blank=False)
#    email = models.CharField(max_length=50, null=True, blank=False)
#
#    class Meta:
#        verbose_name = 'Utilisateur'
#        verbose_name_plural = 'Utilisateurs'

class Post(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False)
    body = models.TextField(null=False, blank=False)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    slug = models.SlugField(default='default-slug')
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def _get_unique_slug(self):
        slug = slugify(self.title)
        unique_slug = slug
        num = 0
        while Post.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        self.slug = self._get_unique_slug()
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'Publication'
        verbose_name_plural = 'Publications'
