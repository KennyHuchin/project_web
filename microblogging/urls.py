from django.urls import path
from microblogging import views
app_name = 'microblogging'


urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register, name='register'),
    path('login/', views.log_in, name='log_in'),
    path('logout/', views.logout_view, name='log_out'),
    path('post/<int:pk>', views.post_detail, name="post_detail"),
    path('newpost/', views.new_post, name='new_post'),
    path('delete/<int:pk>', views.post_delete, name="post_delete")
]