# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from microblogging.models import *

# Register your models here.

class PostAdmin(admin.ModelAdmin):
    list_display = ( 'title', 'author', 'creation_date', 'update_date',)
    search_fields = ('author', )
    list_filter = ('creation_date', )


admin.site.register(Post, PostAdmin)